project = 'JHBuild'
copyright = '2023, JHBuild Contributors'
author = 'James Henstridge'


extensions = []

templates_path = ['_templates']
exclude_patterns = []

html_theme = 'alabaster'
html_static_path = ['_static']
